import torch
import torch.nn as nn
import torch.nn.functional as F





class Unet3d_lite(nn.Module):
    def __init__(self, num_features):
        super(Unet3d_lite, self).__init__()

        self.num_features = num_features

        #---------------------------------------------------------------------------------
        self.conv0 = nn.Conv3d(in_channels = 1,
                               out_channels = 32,
                               kernel_size = (1,3,3),
                               padding = (0,1,1),
                               stride = 1,
                               groups = 1)

        self.conv1 = nn.Conv3d(in_channels = 32,
                               out_channels = 32,
                               kernel_size = (1,3,3),
                               padding = (0,1,1),
                               stride = 1,
                               groups = 1)

        self.down0 = nn.MaxPool3d( kernel_size = 1,
                                   stride = None,
                                   padding = 0,
                                   return_indices=True )

        # ---------------------------------------------------------------------------------
        self.conv2 = nn.Conv3d( in_channels = 32,
                                out_channels = 32,
                                kernel_size = (1, 3, 3),
                                padding = (0,1,1),
                                stride = 1,
                                groups = 1 )

        self.conv3 = nn.Conv3d( in_channels = 32,
                                out_channels = 32,
                                kernel_size = (1, 3, 3),
                                padding = (0,1,1),
                                stride = 1,
                                groups = 1 )

        self.down1 = nn.MaxPool3d( kernel_size = 1,
                                   stride = None,
                                   padding = 0,
                                   return_indices = True )

        # ---------------------------------------------------------------------------------
        self.conv4 = nn.Conv3d( in_channels = 32,
                                out_channels = 32,
                                kernel_size = (1, 3, 3),
                                padding = (0,1,1),
                                stride = 1,
                                groups = 1)

        self.conv5 = nn.Conv3d( in_channels = 32,
                                out_channels = 32,
                                kernel_size = (1, 3, 3),
                                padding = (0,1,1),
                                stride = 1,
                                groups = 1 )

        self.down2 = nn.MaxPool3d( kernel_size = 1,
                                   stride = None,
                                   padding = 0,
                                   return_indices = True )

        # ---------------------------------------------------------------------------------
        self.conv6 = nn.Conv3d( in_channels = 32,
                                out_channels = 32,
                                kernel_size = (1, 3, 3),
                                padding = (0,1,1),
                                stride = 1,
                                groups = 1 )

        self.conv7 = nn.Conv3d( in_channels = 32,
                                out_channels = 32,
                                kernel_size = (1, 3, 3),
                                padding = (0,1,1),
                                stride = 1,
                                groups = 1 )

        # --------------------------------------------------------------------------------
        self.up0 = nn.MaxUnpool3d( kernel_size = 1,
                                   stride = None,
                                   padding = 0)

        self.mconv0 = nn.Conv3d( in_channels =  32,
                                 out_channels =  32,
                                 kernel_size = (1, 3, 3),
                                 padding = (0,1,1),
                                 stride = 1,
                                 groups = 1 )

        self.mconv1 = nn.Conv3d( in_channels = 32,
                                 out_channels = 32,
                                 kernel_size = (1, 3, 3),
                                 padding = (0,1,1),
                                 stride = 1,
                                 groups = 1)

        # --------------------------------------------------------------------------------
        self.up1 = nn.MaxUnpool3d( kernel_size = 1,
                                   stride = None,
                                   padding = 0)

        self.mconv2 = nn.Conv3d( in_channels =  32,
                                 out_channels =  32,
                                 kernel_size = (1, 3, 3),
                                 padding = (0,1,1),
                                 stride = 1,
                                 groups = 1 )

        self.mconv3 = nn.Conv3d( in_channels = 32,
                                 out_channels = 32,
                                 kernel_size = (1, 3, 3),
                                 padding = (0,1,1),
                                 stride = 1,
                                 groups = 1)

        # --------------------------------------------------------------------------------
        self.up2 = nn.MaxUnpool3d( kernel_size = 1,
                                   stride = None,
                                   padding = 0)

        self.mconv4 = nn.Conv3d( in_channels =  32,
                                 out_channels =  32,
                                 kernel_size = (1, 3, 3),
                                 padding = (0,1,1),
                                 stride = 1,
                                 groups = 1 )

        self.mconv5 = nn.Conv3d( in_channels = 32,
                                 out_channels = 32,
                                 kernel_size = (1, 3, 3),
                                 padding = (0,1,1),
                                 stride = 1,
                                 groups = 1)

        # --------------------------------------------------------------------------------
        self.barr = nn.Conv3d( in_channels = 32,
                               out_channels = self.num_features,
                               kernel_size = (1,3,3),
                               padding = (0,1,1),
                               stride = 1,
                               groups = 1 )


        self.softmax = F.softmax


    def forward(self, x):

        # --------------------------------------------------------------------------------
        x = F.relu( self.conv0( x ) )
        x = F.relu( self.conv1( x ) )
        x, indices_0 = self.down0( x )

        # --------------------------------------------------------------------------------
        x = F.relu( self.conv2( x ) )
        x = F.relu( self.conv3( x ) )
        x, indices_1 = self.down1( x )


        # --------------------------------------------------------------------------------
        x = F.relu( self.conv4( x ) )
        x = F.relu( self.conv5( x ) )
        x, indices_2 = self.down2( x )

        # --------------------------------------------------------------------------------
        x = F.relu( self.conv6( x ) )
        x = F.relu( self.conv7( x ) )

        # --------------------------------------------------------------------------------
        x = self.up0( x, indices_2 )
        x = F.relu( self.mconv0( x ) )
        x = F.relu( self.mconv1( x ) )

        # --------------------------------------------------------------------------------
        x = self.up1( x, indices_1 )
        x = F.relu( self.mconv2( x ) )
        x = F.relu( self.mconv3( x ) )

        # --------------------------------------------------------------------------------
        x = self.up2( x, indices_0 )
        x = F.relu( self.mconv4( x ) )
        x = F.relu( self.mconv5( x ) )

        # --------------------------------------------------------------------------------
        x = F.relu( self.barr( x ) )

        # put chennels at the begining according
        # to ( num_channels, batch_size, z, x, y )
        #x = x.permute( 1, 0, 2, 3, 4 ).contiguous( )
        x = x.permute(0, 2, 3, 4, 1).contiguous()

        # flatten the output as following: ( num_channels, -1 )
        #x = x.view( self.num_features, -1 )
        x = x.view(x.numel() // self.num_features, self.num_features)

        # compute softmax for the transposed output:  softmax( -1, num_channels )
        #x = self.softmax( torch.t( x ) )
        x = self.softmax(x)

        # the output is 2d vector
        return x