import os
import h5py as hf
import numpy as np
from sklearn import preprocessing


class WrongInputFile( Exception ):
    pass

class WrongKeys( Exception ):
    pass

class Loader( ):
    def __init__( self, file_name, data_key, labels_key, fov, debugging_level = 0 ):

        self.debugging_level = debugging_level

        if not os.path.isfile( file_name ):
            raise WrongInputFile( "the input file '%s' does not exist" % file_name )


        try:
            self.file = hf.File( file_name, "r" )
            self.data = self.file[ data_key][ : ].astype( np.float32 )
            self.labels = self.file[ labels_key ][ : ].astype( np.float32 )

            if self.debugging_level == 3:
                print ("reading the data: done")

        except:
            raise WrongKeys( "the input file '%s' does not contain"
                             "one of the following keys: %s, %s"
                             % (file_name, data_key, labels_key) )



        # adjust axes according to PyTorch documentation
        self.data =  np.swapaxes( self.data, axis1 = 1, axis2 = 2 )
        self.labels = np.swapaxes( self.labels, axis1 = 1, axis2 = 2 )
        if self.debugging_level == 3:
            print ("axes adjustment: done")

        # normalize the data
        self.data /= np.max( self.data )
        if self.debugging_level == 3:
            print ("normalization of the raw data: done")


        # define properties of the data set
        self.file_name = file_name
        self.data_key = data_key
        self.labels_key = labels_key
        self.fov = fov
        self.fov_inted = np.array( fov, dtype = np.uint16 ) // 2
        self.data_shape = self.data.shape

        self._encode_one_to_k( )


        # define the max and min values along each axis
        # that extremes are needed to define a sample within the data set which boarders
        # are still within the data set. Avoidance of out of the boundary memory access
        self.z_extremes = { "min" : self.fov_inted[ 0 ] + 1,
                            "max" : self.data_shape[ 0 ] - self.fov_inted[ 0 ] - 1 }

        self.y_extremes = { "min" : self.fov_inted[ 1 ] + 1,
                            "max": self.data_shape[ 1 ] - self.fov_inted[ 1 ] - 1 }

        self.x_extremes = { "min" : self.fov_inted[ 2 ] + 1,
                            "max": self.data_shape[ 2 ] - self.fov_inted[ 2 ] - 1 }

        if self.debugging_level == 3:
            print ("loader initialization: done")


    def _encode_one_to_k(self):

        # define encoder
        encoder = preprocessing.LabelEncoder( )

        # define number of classes
        self.num_features = np.unique( self.labels ).size

        if self.debugging_level == 3:
            print ("DEBUGGING: number of classes", self.num_features)

        # reshape labels to feed to the encoder
        labels = self.labels.reshape( -1 )

        # make encoding
        encoder.fit( labels )
        encoding_result = encoder.transform( labels )

        # rewrite the lables
        self.labels = encoding_result.reshape( self.data_shape ).astype( np.float32 )

        if self.debugging_level == 3:

            print ( "lables parameters after encoding",
                    np.unique(self.labels, return_counts = True) )

            print ("encoding: done")


    def get_sample(self):

        # throw a seed
        x = np.random.randint(low = self.x_extremes["min"], high = self.x_extremes["max"])
        y = np.random.randint(low = self.y_extremes["min"], high = self.y_extremes["max"])
        z = np.random.randint(low = self.z_extremes["min"], high = self.z_extremes["max"])
        self.seed = np.array([ z, y, x ], dtype = np.uint16)


        # define the slicing
        left_bottom_corner = (self.seed - self.fov_inted)
        right_top_corner = (self.seed + self.fov_inted)


        # define the raw data sample
        data_sample = self.data[ left_bottom_corner[ 0 ] : right_top_corner[ 0 ] + 1,
                                 left_bottom_corner[ 1 ] : right_top_corner[ 1 ] + 1,
                                 left_bottom_corner[ 2 ] : right_top_corner[ 2 ] + 1 ]


        # define the label data sample
        lable_sample = self.labels[ left_bottom_corner[ 0 ] : right_top_corner[ 0 ] + 1,
                                    left_bottom_corner[ 1 ] : right_top_corner[ 1 ] + 1,
                                    left_bottom_corner[ 2 ] : right_top_corner[ 2 ] + 1 ]

        if self.debugging_level == 3:
            print ("extraction of data around the seed ( %i, %i, %i ): done"
                   % ( self.seed[0], self.seed[1], self.seed[2] ) )

        return data_sample, lable_sample


    def get_specific_sample(self, x, y, z):

        # throw a seed
        self.seed = np.array([ z, y, x ], dtype = np.uint16)

        # define the slicing
        left_bottom_corner = (self.seed - self.fov_inted)
        right_top_corner = (self.seed + self.fov_inted)


        # define the raw data sample
        data_sample = self.data[ left_bottom_corner[ 0 ] : right_top_corner[ 0 ] + 1,
                                 left_bottom_corner[ 1 ] : right_top_corner[ 1 ] + 1,
                                 left_bottom_corner[ 2 ] : right_top_corner[ 2 ] + 1 ]


        # define the label data sample
        lable_sample = self.labels[ left_bottom_corner[ 0 ] : right_top_corner[ 0 ] + 1,
                                    left_bottom_corner[ 1 ] : right_top_corner[ 1 ] + 1,
                                    left_bottom_corner[ 2 ] : right_top_corner[ 2 ] + 1 ]

        if self.debugging_level == 3:
            print ("extraction of data around the seed ( %i, %i, %i ): done"
                   % ( self.seed[0], self.seed[1], self.seed[2] ) )

        return data_sample, lable_sample



    def get_batch_sample(self, batch_size ):
        num_channels = 1
        batch_data_sample = np.zeros( ([ batch_size, num_channels ] + self.fov), dtype =np.float32 )
        batch_labels_sample = np.zeros( ( [ batch_size ] + self.fov ), dtype =np.float32 )

        for i in range( batch_size ):

            data_sample, labels_sample = self.get_sample()

            batch_data_sample[ i ][ 0 ] = data_sample
            batch_labels_sample[ i ] = labels_sample

        return batch_data_sample, batch_labels_sample


"""
Example:
myLoader = Loader( file_name = "./Data/test_data.h5",
                   data_key = "em_raw" ,
                   labels_key = "labels",
                   fov = [ 22, 139, 139 ] )

sample_data, sample_lable = myLoader.get_sample()
"""

