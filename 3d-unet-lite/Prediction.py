import torch

from torch.autograd import Variable
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F


from Model import Unet3d_lite
import h5py as hf
import numpy as np

from Loader import *

fov = [ 23, 139, 139 ]

loader = Loader( file_name = "./Data/test_data.h5",
                 data_key = "em_raw" ,
                 labels_key = "labels",
                 fov = fov,
                 debugging_level = 3 )


# define the model
model_param_id = 280
net = Unet3d_lite(num_features = loader.num_features)

checkpoint = torch.load( 'unet-weights-%d.pt' % model_param_id )
net.load_state_dict( checkpoint[ 'state_dict' ] )

# make prediction
print ("run prediction")

# load the cube
input, target = loader.get_specific_sample( x = 75, y = 75, z = 75 )
tr_input = input[np.newaxis, np.newaxis, :]

# load data to GPU
torch_inputs = Variable( torch.from_numpy( tr_input ) )

outputs = net(torch_inputs).data.numpy()

prediction = 255 * np.argmax( outputs, axis = 1 )
prediction = prediction.reshape( fov )


print ("DEBUGGING: number of distinct features:", np.unique(prediction))
import h5py as hv
file = hv.File("output.h5", "w")
file.create_dataset( "predict", data = prediction, dtype = np.uint8 )
file.close()