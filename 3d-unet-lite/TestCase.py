from __future__ import print_function
import torch

from torch.autograd import Variable
import torch.optim as optim
import torch.nn as nn

import matplotlib.pyplot as plt
from Model import Unet3d_lite
from Queue import Queue
from Loader import *
import h5py as hf
import numpy as np
import sys
import os

DEBUGGING_LEVEL = 1

# set up the imput and labal data
# read the entire data set
loader = Loader( file_name = "./Data/test_data.h5",
                 data_key = "em_raw" ,
                 labels_key = "labels",
                 fov = [ 13, 47, 47 ],
                 debugging_level = 1 )

print ("init loader: done")

# define the model
net = Unet3d_lite(num_features = loader.num_features).cuda()


# define the basic parameters
counter = 0
learning_rate = 1e-5
batch_size = 35
num_runs_per_sample = 2
num_iterations_per_save = 5000
mean_loss = 1.0
tolerance = 0.1

#print ("weights initialization: done")

if len(sys.argv) == 2:

    file_model_name = sys.argv[ 1 ]

    if os.path.isfile( file_model_name ):
        print( "=> loading checkpoint '{}'".format( file_model_name ) )

        checkpoint = torch.load( file_model_name )
        counter = checkpoint[ 'counter' ]
        learning_rate = checkpoint[ 'learning_rate' ]
        batch_size = checkpoint[ 'batch_size' ]
        num_runs_per_sample = checkpoint[ 'num_runs_per_sample' ]
        num_iterations_per_save = checkpoint[ 'num_iterations_per_save' ]
        net.load_state_dict( checkpoint[ 'state_dict' ] )

        print( "    the following parameters have been loaded: ")
        print( "    counter: %d" % counter )
        print( "    learning_rate: %f" % learning_rate )
        print( "    bach_size: %d" % batch_size )
        print( "    num_runs_per_sample: %d" % num_runs_per_sample )
        print( "    num_iterations_per_save: %d" % num_iterations_per_save )



# define MultiNoulli as our loss function.
weights = torch.Tensor( [ 1.0, 1.0 ] )
criterion = nn.NLLLoss( weight=weights ).cuda()

# define optimizer
optimizer = optim.Adam(net.parameters(), lr=learning_rate)
conrainer = Queue( maxsize = 1000 )
print ("run training")

iterator = 0
while mean_loss > tolerance:


    try:
        mean_loss = 0.0
        iterator += 1

        # get a sample from the loader
        tr_input, tr_target = loader.get_batch_sample( batch_size = batch_size )

        # load data to GPU
        torch_inputs = Variable( torch.from_numpy( tr_input ).cuda() )

        # convert labeles to long interger
        torch_target = Variable( torch.from_numpy( tr_target ).cuda() ).long()

        # flatten the targets as 1d vector
        torch_flatten_target = torch_target.view( -1 )

        for i in range( num_runs_per_sample ):

            # feed the network with input data
            outputs = net(torch_inputs)

            # compute loss
            loss = criterion(torch.log(outputs), torch_flatten_target)

            # clean the gradient from the previous computations
            optimizer.zero_grad( )

            # do back propagation
            loss.backward()

            # make one step towards the optimum
            optimizer.step()

            mean_loss += loss.data.cpu().numpy()

            if conrainer.full():
                conrainer.queue.clear()

            conrainer.put( loss.data.cpu( ).numpy( ) )


        # print the loss value
        print ("#iteration: ", counter, "   loss:", loss.data.cpu().numpy())

        # save state
        if counter % num_iterations_per_save == 0:

            print( "saving the current state..." )
            torch.save( { 'counter' : counter + 1,
                          'learning_rate': learning_rate,
                          'batch_size': batch_size,
                          'num_runs_per_sample': num_runs_per_sample,
                          'num_iterations_per_save': num_iterations_per_save,
                          'state_dict': net.state_dict( ) },
                        'unet-weights-%d.pt' % counter )



        # update flags
        counter += 1
        mean_loss /= num_runs_per_sample


    except KeyboardInterrupt:

        print ("\n\n\n  MENU:")

        while True:
            print('--> continue \n'
                  '--> save \n'
                  '--> unique \n'
                  '--> learning rate \n'
                  '--> batch size \n'
                  '--> save sample \n'
                  '--> convergence \n'
                  '--> filters \n'
                  '--> num runs per batch \n'
                  '--> num iterations per save \n'
                  '--> abort \n')

            answer = raw_input('option: ')

            if ( answer == "continue" ) or \
               ( answer == "save" ) or \
               ( answer == "unique" ) or \
               ( answer == "learning rate" ) or \
               ( answer == "batch size" ) or \
               ( answer == "save sample" ) or \
               ( answer == "convergence" ) or \
               ( answer == "filters" ) or \
               ( answer == "num runs per batch" ) or \
               ( answer == "num iterations per save" ) or \
               ( answer == "abort" ):

                break


        if answer == "save":

            # save state of the model
            torch.save( { 'counter' : counter + 1,
                          'learning_rate': learning_rate,
                          'batch_size' : batch_size,
                          'num_runs_per_sample': num_runs_per_sample,
                          'num_iterations_per_save': num_iterations_per_save,
                          'state_dict': net.state_dict( ) },
                        'unet-weights-%d.pt' % counter )


        if answer == "unique":

            print ("number of unique predicted classes: processing")
            # take output and transfer it to the host cpu
            # convert the output to numpy array and take the artmax
            # to get the predicted cell ids
            output_test = net(torch_inputs).cpu().data.numpy()
            prediction = np.argmax(output_test, axis=1)
            np.savetxt("debugging", output_test)

            # compute unique ids
            print (np.unique(prediction, return_counts = True))


        if answer == "learning rate":

            print( "current learning rate value: %f" % learning_rate )

            while True:
                try:
                    temp = raw_input("set up the learning rate for the optimizer: ")
                    learning_rate = float( temp )

                    for param_group in optimizer.param_groups:
                        param_group['lr'] = learning_rate


                    break
                except ValueError:
                    print("\n\n\n ERROR ===> wrong input \n\n\n")


        if answer == "batch size":
            print( "current batch size: %d" % batch_size )

            while True:
                try:

                    temp = raw_input("set up the batch size: ")
                    batch_size = int( temp )
                    break
                except ValueError:
                    print("\n\n\n ERROR ===> wrong input \n\n\n")


        if answer == "save sample":

            file_name = raw_input( 'enter file name: ' )
            file_h5py = hf.File( ( file_name + ".h5" ), "w" )

            file_h5py.create_dataset("raw_data", data = torch_inputs.cpu().data.numpy()[ 0 ][ 0 ],
                                     dtype = np.float32 )

            file_h5py.create_dataset( "lables", data = torch_target.cpu().data.numpy()[ 0 ],
                                      dtype = np.float32 )

            file_h5py.close()


        if answer == "convergence":

            plt.plot( list( conrainer.queue ) )
            plt.show()


        if answer == "filters":

            if not os.path.isdir( "./filters" ):
                os.system("mkdir ./filters")
            else:
                os.system( "rm -f ./filters/*" )

            for index, layer in enumerate( net.modules( ) ):
                if type( layer ) == nn.Conv3d:
                    print( index, '->', layer )
                    #print (m.weight.cpu().data.numpy())

                    weights = layer.weight.cpu( ).data.numpy( )
                    weights = np.squeeze( weights )


                    num_filters_in_conv_layer = weights.shape[ 0 ]
                    num_filters_per_line = np.floor( np.sqrt( num_filters_in_conv_layer )) + 1

                    for i in range( 1, num_filters_in_conv_layer + 1):
                        plt.subplot( num_filters_per_line, num_filters_per_line, i )
                        plt.axis( 'off' )
                        plt.imshow( weights[ i - 1 ], interpolation='bilinear' )

                    plt.savefig("./filters/Conv%d.png" % index)


        if answer == "num runs per batch":

             print( "current number of runs: %d" % num_runs_per_sample )

             while True:
                try:
                    temp = raw_input("set up the number iterations per batch sample: ")
                    num_runs_per_sample = int( temp )
                    break
                except ValueError:
                    print("\n\n\n ERROR ===> wrong input \n\n\n")


        if answer == "num iterations per save":
            print( "current iterations per saving: %d" % num_iterations_per_save )

            while True:
                try:

                    temp = raw_input( "set up number iterations"
                                                 "per saving: " )

                    num_iterations_per_save = int( temp )
                    break
                except ValueError:
                    print( "\n\n\n ERROR ===> wrong input \n\n\n" )


        if answer == "abort":
            sys.exit(0)

        mean_loss = 1.0

